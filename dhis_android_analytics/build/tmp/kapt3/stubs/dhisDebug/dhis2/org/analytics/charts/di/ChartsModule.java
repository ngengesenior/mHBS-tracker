package dhis2.org.analytics.charts.di;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0001\u00a2\u0006\u0002\b\u0007J\u0015\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0001\u00a2\u0006\u0002\b\fJ\r\u0010\r\u001a\u00020\u000eH\u0001\u00a2\u0006\u0002\b\u000fJ\u0015\u0010\u0010\u001a\u00020\u00112\u0006\u0010\n\u001a\u00020\u000bH\u0001\u00a2\u0006\u0002\b\u0012J\r\u0010\u0013\u001a\u00020\u0014H\u0001\u00a2\u0006\u0002\b\u0015J\r\u0010\u0016\u001a\u00020\u0017H\u0001\u00a2\u0006\u0002\b\u0018J\u001d\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u00142\u0006\u0010\u001c\u001a\u00020\u0017H\u0001\u00a2\u0006\u0002\b\u001dJ-\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u001a2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\b\u001a\u00020\tH\u0001\u00a2\u0006\u0002\b!J-\u0010\"\u001a\u00020#2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010$\u001a\u00020\u001f2\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020(H\u0001\u00a2\u0006\u0002\b)J\u001d\u0010*\u001a\u00020&2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\b\u001a\u00020\tH\u0001\u00a2\u0006\u0002\b+J\u001d\u0010,\u001a\u00020(2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\b\u001a\u00020\tH\u0001\u00a2\u0006\u0002\b-\u00a8\u0006."}, d2 = {"Ldhis2/org/analytics/charts/di/ChartsModule;", "", "()V", "bindStorageFeatureImpl", "Ldhis2/org/analytics/charts/Charts;", "analyticsCharts", "Ldhis2/org/analytics/charts/DhisAnalyticCharts;", "bindStorageFeatureImpl$dhis_android_analytics_dhisDebug", "chartCoordinatesProvider", "Ldhis2/org/analytics/charts/providers/ChartCoordinatesProvider;", "d2", "Lorg/hisp/dhis/android/core/D2;", "chartCoordinatesProvider$dhis_android_analytics_dhisDebug", "nutritionDataProvider", "Ldhis2/org/analytics/charts/providers/NutritionDataProvider;", "nutritionDataProvider$dhis_android_analytics_dhisDebug", "periodStepProvider", "Ldhis2/org/analytics/charts/providers/PeriodStepProvider;", "periodStepProvider$dhis_android_analytics_dhisDebug", "provideAnalyticDataElementMapper", "Ldhis2/org/analytics/charts/mappers/AnalyticDataElementToDataElementData;", "provideAnalyticDataElementMapper$dhis_android_analytics_dhisDebug", "provideAnalyticIndicatorMapper", "Ldhis2/org/analytics/charts/mappers/AnalyticIndicatorToIndicatorData;", "provideAnalyticIndicatorMapper$dhis_android_analytics_dhisDebug", "provideAnalyticSettingsMapper", "Ldhis2/org/analytics/charts/mappers/AnalyticTeiSettingsToSettingsAnalyticsModel;", "analyticDataElementMapper", "analyticIndicatorMapper", "provideAnalyticSettingsMapper$dhis_android_analytics_dhisDebug", "provideAnalyticsSettingsToGraph", "Ldhis2/org/analytics/charts/mappers/AnalyticsTeiSettingsToGraph;", "analyticsSettingsMapper", "provideAnalyticsSettingsToGraph$dhis_android_analytics_dhisDebug", "provideChartRepository", "Ldhis2/org/analytics/charts/ChartsRepository;", "analyticsTeiSettingsToGraph", "dataElementToGraph", "Ldhis2/org/analytics/charts/mappers/DataElementToGraph;", "indicatorToGraph", "Ldhis2/org/analytics/charts/mappers/ProgramIndicatorToGraph;", "provideChartRepository$dhis_android_analytics_dhisDebug", "provideDataElementToGraph", "provideDataElementToGraph$dhis_android_analytics_dhisDebug", "provideIndicatorToGraph", "provideIndicatorToGraph$dhis_android_analytics_dhisDebug", "dhis_android_analytics_dhisDebug"})
@dagger.Module()
public final class ChartsModule {
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final dhis2.org.analytics.charts.ChartsRepository provideChartRepository$dhis_android_analytics_dhisDebug(@org.jetbrains.annotations.NotNull()
    org.hisp.dhis.android.core.D2 d2, @org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.mappers.AnalyticsTeiSettingsToGraph analyticsTeiSettingsToGraph, @org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.mappers.DataElementToGraph dataElementToGraph, @org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.mappers.ProgramIndicatorToGraph indicatorToGraph) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final dhis2.org.analytics.charts.mappers.AnalyticsTeiSettingsToGraph provideAnalyticsSettingsToGraph$dhis_android_analytics_dhisDebug(@org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.mappers.AnalyticTeiSettingsToSettingsAnalyticsModel analyticsSettingsMapper, @org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.providers.NutritionDataProvider nutritionDataProvider, @org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.providers.PeriodStepProvider periodStepProvider, @org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.providers.ChartCoordinatesProvider chartCoordinatesProvider) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final dhis2.org.analytics.charts.providers.NutritionDataProvider nutritionDataProvider$dhis_android_analytics_dhisDebug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final dhis2.org.analytics.charts.mappers.AnalyticTeiSettingsToSettingsAnalyticsModel provideAnalyticSettingsMapper$dhis_android_analytics_dhisDebug(@org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.mappers.AnalyticDataElementToDataElementData analyticDataElementMapper, @org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.mappers.AnalyticIndicatorToIndicatorData analyticIndicatorMapper) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final dhis2.org.analytics.charts.mappers.DataElementToGraph provideDataElementToGraph$dhis_android_analytics_dhisDebug(@org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.providers.PeriodStepProvider periodStepProvider, @org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.providers.ChartCoordinatesProvider chartCoordinatesProvider) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final dhis2.org.analytics.charts.mappers.ProgramIndicatorToGraph provideIndicatorToGraph$dhis_android_analytics_dhisDebug(@org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.providers.PeriodStepProvider periodStepProvider, @org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.providers.ChartCoordinatesProvider chartCoordinatesProvider) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final dhis2.org.analytics.charts.providers.PeriodStepProvider periodStepProvider$dhis_android_analytics_dhisDebug(@org.jetbrains.annotations.NotNull()
    org.hisp.dhis.android.core.D2 d2) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final dhis2.org.analytics.charts.providers.ChartCoordinatesProvider chartCoordinatesProvider$dhis_android_analytics_dhisDebug(@org.jetbrains.annotations.NotNull()
    org.hisp.dhis.android.core.D2 d2) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final dhis2.org.analytics.charts.mappers.AnalyticDataElementToDataElementData provideAnalyticDataElementMapper$dhis_android_analytics_dhisDebug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final dhis2.org.analytics.charts.mappers.AnalyticIndicatorToIndicatorData provideAnalyticIndicatorMapper$dhis_android_analytics_dhisDebug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final dhis2.org.analytics.charts.Charts bindStorageFeatureImpl$dhis_android_analytics_dhisDebug(@org.jetbrains.annotations.NotNull()
    dhis2.org.analytics.charts.DhisAnalyticCharts analyticsCharts) {
        return null;
    }
    
    public ChartsModule() {
        super();
    }
}